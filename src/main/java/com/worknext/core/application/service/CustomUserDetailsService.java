package com.worknext.core.application.service;

import com.worknext.core.application.model.CustomUserDetails;
import com.worknext.core.application.model.WebAccount;
import com.worknext.core.application.repository.WebAccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    private static final Logger logger = LoggerFactory.getLogger(CustomUserDetailsService.class);

    private final WebAccountRepository webAccountRepository;

    public CustomUserDetailsService(WebAccountRepository webAccountRepository) {
        this.webAccountRepository = webAccountRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        logger.info("retrieving by username..");
        WebAccount webAccount = this.webAccountRepository.findByUsername(username)
            .orElseThrow(() -> new RuntimeException("Could not find profile linked to: " + username));

        logger.info("account found: " + webAccount.getUsername());

        CustomUserDetails userDetails = new CustomUserDetails(webAccount);
        logger.info("User: " + webAccount.getUsername());
        logger.info("userDetails: " + userDetails.getAuthorities());
        return userDetails;

    }
}
