package com.worknext.core.application.service;

import com.worknext.core.application.dto.GenericResponse;
import com.worknext.core.application.model.WebAccount;
import com.worknext.core.application.repository.WebAccountRepository;
import com.worknext.core.config.provider.VerificationTokenProvider;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class WebAccountVerificationServiceImpl implements WebAccountVerificationService{

    private VerificationTokenProvider verificationTokenProvider;
    private WebAccountRepository webAccountRepository;
    private MailSenderService mailSenderService;

    public WebAccountVerificationServiceImpl(VerificationTokenProvider verificationTokenProvider,
                                             MailSenderService mailSenderService,
                                             WebAccountRepository webAccountRepository) {
        this.verificationTokenProvider = verificationTokenProvider;
        this.mailSenderService = mailSenderService;
        this.webAccountRepository = webAccountRepository;
    }

    @Override
    public String createVerificationToken(final String emailAddress) {
        return this.verificationTokenProvider.generateToken(emailAddress);
    }

    @Override
    @Async
    public void sendVerification(String email, String token) {
        this.mailSenderService.sendVerificationEmail(email, token);
    }

    @Override
    public GenericResponse<Boolean> verifyWebAccount(String token) {
        if (verificationTokenProvider.validateToken(token)) {
            final String emailAddress = (String) this.verificationTokenProvider.getEmailAddress(token);

            final WebAccount webAccount = this.webAccountRepository.getByEmailAddress(emailAddress)
                .orElseThrow(() -> new RuntimeException("Verification Error: Could not find email address: "
                    + emailAddress));

            webAccount.setVerified(true);
            this.webAccountRepository.save(webAccount);

            return new GenericResponse<>("Verification Success", true);
        }
        return new GenericResponse<>("Verification Failed", false);
    }

}
