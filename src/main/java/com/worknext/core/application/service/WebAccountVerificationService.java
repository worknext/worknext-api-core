package com.worknext.core.application.service;

import com.worknext.core.application.dto.GenericResponse;

public interface WebAccountVerificationService {
    String createVerificationToken(String emailAddress);

    void sendVerification(String email, String token);

    GenericResponse<Boolean> verifyWebAccount(String token);
}
