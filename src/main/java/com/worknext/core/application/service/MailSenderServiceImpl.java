package com.worknext.core.application.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class MailSenderServiceImpl implements MailSenderService{

    private static final Logger logger = LoggerFactory.getLogger(MailSenderServiceImpl.class);
    private JavaMailSender javaMailSender;

    public MailSenderServiceImpl(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void sendVerificationEmail(String recipientEmail, String token) {
        final SimpleMailMessage simpleMailMessage = new SimpleMailMessage();

        simpleMailMessage.setFrom("red.dev.things@gmail.com");
        simpleMailMessage.setTo(recipientEmail);
        simpleMailMessage.setSubject("WorkNext Email Verification");
        simpleMailMessage.setText("Verify your email: http://localhost:8080/api/v1/auth/verify?token=" + token);

        logger.info("Sending verification mail for: " + recipientEmail);

        javaMailSender.send(simpleMailMessage);
        logger.info("Verification Mail Sent for: " + recipientEmail);
    }
}
