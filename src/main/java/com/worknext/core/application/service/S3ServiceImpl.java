package com.worknext.core.application.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.PutObjectResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

@Service
public class S3ServiceImpl implements S3Service {

    private static final Logger logger = LoggerFactory.getLogger(S3ServiceImpl.class);

    @Value("${aws.s3.bucket.worknext}")
    private String bucketName;

    private final AmazonS3 s3;

    public S3ServiceImpl(final AmazonS3 s3) {
        this.s3 = s3;
    }

    @Override
    public String saveFile(MultipartFile multipartFile, String filename) {

        try {
            final PutObjectResult putObjectResult = s3.putObject(
                bucketName,
                filename,
                convertToFile(multipartFile, filename));
            return putObjectResult.getContentMd5();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }

        return null;
    }

    @Override
    public byte[] downloadFile(String filename) {
        return new byte[0];
    }

    @Override
    public String deleteFile(String filename) {
        return null;
    }

    @Override
    public List<String> listFiles() {
        return null;
    }

    private File convertToFile(final MultipartFile multipartFile, final String filename) throws IOException {
        File file = new File(Objects.requireNonNull(filename));
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(multipartFile.getBytes());
        fos.close();

        return file;

    }
}
