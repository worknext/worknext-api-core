package com.worknext.core.application.service;

import com.worknext.core.application.dto.WebAccountDTO;
import com.worknext.core.application.model.WebAccount;
import com.worknext.core.application.repository.WebAccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class WebAccountServiceImpl implements WebAccountService {

    private static final Logger logger = LoggerFactory.getLogger(WebAccountServiceImpl.class);

    private final WebAccountRepository repository;
    private final WebAccountVerificationService webAccountVerificationService;
    private final PasswordEncoder passwordEncoder;

    public WebAccountServiceImpl(WebAccountRepository repository,
                                 WebAccountVerificationService webAccountVerificationService,
                                 PasswordEncoder passwordEncoder) {
        this.repository = repository;
        this.webAccountVerificationService = webAccountVerificationService;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public boolean registerUser(final WebAccountDTO webAccountDTO) {
        final long existingEmailCount = this.repository.countByEmailAddress(webAccountDTO.getEmail());

        if (existingEmailCount < 1) {
            final WebAccount account = new WebAccount(
                webAccountDTO.getUsername(),
                webAccountDTO.getEmail(),
                this.passwordEncoder.encode(webAccountDTO.getPassword()),
                false,
                webAccountDTO.getAccountType()
            );

            repository.save(account);

            logger.info("Account Created.");

            final String verificationToken = this.webAccountVerificationService
                .createVerificationToken(webAccountDTO.getEmail());

            this.webAccountVerificationService.sendVerification(webAccountDTO.getEmail(), verificationToken);
            return true;
        }

        throw new RuntimeException("Email Address is already taken");
    }

}
