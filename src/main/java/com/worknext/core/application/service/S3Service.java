package com.worknext.core.application.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface S3Service {

    String saveFile(MultipartFile multipartFile, String filename);

    byte[] downloadFile(final String filename);

    String deleteFile(final String filename);

    List<String> listFiles();

}
