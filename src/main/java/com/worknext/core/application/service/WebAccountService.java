package com.worknext.core.application.service;

import com.worknext.core.application.dto.WebAccountDTO;

public interface WebAccountService {
    boolean registerUser(WebAccountDTO webAccountDTO);
}
