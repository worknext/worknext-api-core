package com.worknext.core.application.service;

public interface MailSenderService {
    void sendVerificationEmail(String recipientEmail, String token);
}
