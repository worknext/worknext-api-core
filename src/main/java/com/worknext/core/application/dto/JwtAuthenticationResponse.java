package com.worknext.core.application.dto;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class JwtAuthenticationResponse {
    private String accessToken;
    private String tokenType = "Bearer";
    private Collection<? extends GrantedAuthority> auths;

    public JwtAuthenticationResponse(String accessToken, Collection<? extends GrantedAuthority> auths) {
        this.accessToken = accessToken;
        this.auths = auths;

    }

    public Collection<? extends GrantedAuthority> getAuths() {
        return auths;
    }

    public void setAuths(Collection<? extends GrantedAuthority> auths) {
        this.auths = auths;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

}
