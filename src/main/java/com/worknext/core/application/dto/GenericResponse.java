package com.worknext.core.application.dto;

public class GenericResponse<T> {

    private final String message;
    private final T data;

    public GenericResponse(String message, T data) {
        this.message = message;
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public T getData() {
        return data;
    }
}
