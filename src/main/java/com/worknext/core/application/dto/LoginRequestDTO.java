package com.worknext.core.application.dto;

public class LoginRequestDTO {

    public String username;
    public String password;

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
