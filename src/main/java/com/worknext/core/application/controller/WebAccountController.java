package com.worknext.core.application.controller;

import com.worknext.core.application.dto.GenericResponse;
import com.worknext.core.application.dto.JwtAuthenticationResponse;
import com.worknext.core.application.dto.LoginRequestDTO;
import com.worknext.core.application.dto.WebAccountDTO;
import com.worknext.core.application.service.WebAccountService;
import com.worknext.core.application.service.WebAccountVerificationService;
import com.worknext.core.config.provider.TokenProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RequestMapping("/auth")
@RestController
public class WebAccountController {

    private static Logger logger = LoggerFactory.getLogger(WebAccountController.class);

    private final WebAccountService webAccountService;
    private final AuthenticationManager authenticationManager;
    private final WebAccountVerificationService webAccountVerificationService;
    private final TokenProvider tokenProvider;

    public WebAccountController(WebAccountService webAccountService, AuthenticationManager authenticationManager,
                                WebAccountVerificationService webAccountVerificationService,
                                TokenProvider tokenProvider) {
        this.webAccountService = webAccountService;
        this.authenticationManager = authenticationManager;
        this.webAccountVerificationService = webAccountVerificationService;
        this.tokenProvider = tokenProvider;
    }

    @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public GenericResponse<?> registerAccount(@RequestBody WebAccountDTO webAccountDTO) {
        this.webAccountService.registerUser(webAccountDTO);

        return new GenericResponse("SUCCESS", null);
    }

    @PostMapping(value = "/login", consumes="application/json", produces = "application/json")
    public GenericResponse<JwtAuthenticationResponse> authenticateUser(@RequestBody LoginRequestDTO loginRequest) {
        logger.info("login attempt");
        Authentication authentication = authenticationManager.authenticate(
            new UsernamePasswordAuthenticationToken(
                loginRequest.getUsername(),
                loginRequest.getPassword()
            )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = tokenProvider.generateToken(authentication);

        Collection<? extends GrantedAuthority> auths =
            SecurityContextHolder.getContext().getAuthentication().getAuthorities();

        return new GenericResponse<>(
            "SUCCESS", new JwtAuthenticationResponse(jwt, auths));
    }

    @PostMapping(value = "/verify")
    public GenericResponse<Boolean> emailVerifyUser(@RequestParam("token") String token) {
        return this.webAccountVerificationService.verifyWebAccount(token);
    }

    @GetMapping(value = "/version")
    public GenericResponse<String> getVersion() {
        return new GenericResponse<>("Version", "0.0.4");
    }


}
