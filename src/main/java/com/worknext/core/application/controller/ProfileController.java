package com.worknext.core.application.controller;

import com.worknext.core.application.dto.GenericResponse;
import com.worknext.core.application.service.S3Service;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/profile")
public class ProfileController {

    private S3Service s3Service;

    public ProfileController(S3Service s3Service) {
        this.s3Service = s3Service;
    }

    @PostMapping("")
    public GenericResponse<String> profile(@RequestParam("file") MultipartFile file, Authentication authentication) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        System.out.println("User has authorities: " + userDetails.getUsername());
        String extension = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));

        final String response = this.s3Service.saveFile(file, userDetails.getUsername() + extension);

        return new GenericResponse<>("SUCCESS", response);
    }
}
