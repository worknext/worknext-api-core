package com.worknext.core.application.model;

import javax.persistence.*;

@Entity
public class WebAccount extends AuditableEntity {

    @Column(nullable = false)
    private String username;

    @Column(nullable = false)
    private String emailAddress;

    @Column(nullable = false)
    private String password;

    @Column(nullable = true)
    private boolean verified;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private AccountType accountType;

    public WebAccount() {
    }

    public WebAccount(String username, String emailAddress, String password, boolean verified, AccountType accountType) {
        this.username = username;
        this.emailAddress = emailAddress;
        this.password = password;
        this.verified = verified;
        this.accountType = accountType;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }
}
