package com.worknext.core.application.model;

public enum AccountType {
    FREELANCER, CLIENT
}
