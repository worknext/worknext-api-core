package com.worknext.core.application.repository;

import com.worknext.core.application.model.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Optional;

@NoRepositoryBean
public interface ProfileRepository<T extends Profile> extends JpaRepository<T, Long> {

    Optional<T> findByFirstName(String firstName);

}
