package com.worknext.core.application.repository;

import com.worknext.core.application.model.WebAccount;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface WebAccountRepository extends JpaRepository<WebAccount, Long> {

    Optional<WebAccount> findByUsername(String username);

    long countByEmailAddress(String emailAddress);

    Optional<WebAccount> getByEmailAddress(String emailAddress);

}
