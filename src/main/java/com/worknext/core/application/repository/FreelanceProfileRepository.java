package com.worknext.core.application.repository;

import com.worknext.core.application.model.FreelanceProfile;

public interface FreelanceProfileRepository extends ProfileRepository<FreelanceProfile> {


}
