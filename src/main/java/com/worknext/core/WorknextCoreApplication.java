package com.worknext.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories
@SpringBootApplication
public class WorknextCoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(WorknextCoreApplication.class, args);
	}

}
