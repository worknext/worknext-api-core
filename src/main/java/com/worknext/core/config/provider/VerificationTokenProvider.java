package com.worknext.core.config.provider;

import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class VerificationTokenProvider {

    private static final Logger logger = LoggerFactory.getLogger(TokenProvider.class);

    @Value("${app.security.secret}")
    private String secret;

    @Value("${app.security.expiry}")
    private int expiry;

    public String generateToken(String emailAddress) {

        Date date = new Date();
        Date expiryDate = new Date(date.getTime() + expiry);

        return Jwts.builder()
            .setSubject(emailAddress)
            .setIssuedAt(date)
            .setExpiration(expiryDate)
            .claim("for_verify", true)
            .signWith(SignatureAlgorithm.HS512, secret)
            .compact();

    }

    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(secret).parseClaimsJws(authToken);
            return true;
        }catch (ExpiredJwtException e) {
            logger.error("Expired JWT Exception");
        }catch (SignatureException e) {
            logger.error("Invalid JWT signature");
        }
        return false;
    }

    public Object getEmailAddress(String token) {
        final Claims claims = Jwts.parser()
            .setSigningKey(secret)
            .parseClaimsJws(token)
            .getBody();
        return claims.getSubject();
    }
}
